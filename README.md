# Medical Centre Project

This webpage serves as an administration system for a medical clinic.

Features:

    Patients can:
        - Create an account
        - Log in with their credentials
        - Book a visit (if there is no other booking at the same date, time & with the same doctor)
        - See status of past bookings

    Administrator can:
        - Create a doctor
        - Delete a doctor
        - Update a pending booking as confirmed or rejected

    