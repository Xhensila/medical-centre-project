$( document ).ready(function() {

  getDashboardData();

});

function getDashboardData(){
  
  $(".container").remove();
  $("#book_button").remove();
   $(".list-group").html("");
  $("#title").html("DASHBOARD");
  $("#username").html(sessionStorage.getItem("patient_username"));


  $.ajax({
    type: "GET",
    contentType: "application/json",
    url: 'http://localhost:3000/bookings?patient_id='+sessionStorage.getItem("patient_id"),
    dataType: "json",
    success:function(resp){

    
      if(resp.length==0){
        $(".list-group").append( "<li class='list-group-item list-group-item-info'>There is not any pending booking.</li>");
      }
      else{
        for(var i=0;i<resp.length;i++){
  
            var patient_name  =   resp[i].patient_name;
            var date          =   resp[i].date;
            var time          =   resp[i].time;
            var doctor        =   resp[i].doctor;
            var confirmed     =   resp[i].confirmed;
  
            if(confirmed=="1"){
              $(".list-group").append("<li class='list-group-item list-group-item-success'><strong>Patient:</strong> "+ patient_name + " <strong>  Date: </strong>" + date+ "<strong> Time:</strong> "+ time + " <strong> Doctor: </strong> " + doctor +" <strong> Status: </strong> Confirmed  </li>");
            }
            else if(confirmed=="0"){
              $(".list-group").append("<li class='list-group-item list-group-item-danger'><strong>Patient:</strong> "+ patient_name + " <strong>  Date:</strong> " + date+ " <strong> Time: </strong> "+ time + "<strong>  Doctor: </strong> " + doctor + "<strong> Status: </strong> Rejected    </li>");
            }
            else if(confirmed=="pending"){
              $(".list-group").append("<li class='list-group-item list-group-item-warning'><strong>Patient: </strong> "+ patient_name + " <strong>  Date:</strong> " + date+ "<strong>  Time: </strong> "+ time + " <strong> Doctor: </strong> " + doctor + " <strong> Status: </strong> Pending</li>");
            }
  
          }//for
        }//else
      }//success
    })//ajax
  


     $("#visits").append('<button onclick="bookVisit();" id="book_button" type="button" >Book a new visit</button>');

   }  
  





function bookVisit(){

   $(".list-group").html("");
   $("#book_button").remove();
   $("#title").html("BOOK A VISIT");

    var html="";
    html+= '<div class="container">';
    html+='<div class="card-body">';
   

    html+='<div class="form-label-group">';
      html+='<input type="text" id="patient_name" class="form-control" placeholder="Your name" required autofocus>';
      html+='<label for="patient_name"></label>';
    html+='</div>';
    
    html+='<div class="form-label-group">';
      html+='<input type="date" id="date" name="trip-start"  min="2019-01-01" max="2021-12-31" class="form-control" placeholder="Date" required autofocus>';
      html+='<label for="date"></label>';
    html+='</div>';

    html+=' <div class="form-label-group">';
      html+='<select id="time" class="custom-select custom-select-md mb-3">';
        html+='<option disabled selected>Select time</option>';
        html+='<option >09:00</option>';
        html+='<option>10:00</option>';
        html+='<option>11:00</option>';
        html+='<option>12:00</option>';
        html+='<option>13:00</option>';
        html+='<option>14:00</option>';
      html+='</select>';
    html+='</div>';

    html+='<div class="form-label-group">';
      html+='<select id="selectList" class="custom-select custom-select-md mb-3">';
        html+='<option disabled selected>Select doctor</option>';
      html+='</select>';
    html+='</div>';

    html+='<button  id="book"  onclick="bookaNewVisit();" type="submit">Book</button>';
   html+='</div>';
   html+='</div>';
   
$("#visits").append(html);

getDoctors();

}

function getDoctors(){
    
  $.ajax({
    type: "GET",
    contentType: "application/json",
    url: 'http://localhost:3000/doctors',
    dataType: "json",
    success:function(response){
      for(var i=0;i<response.length;i++){
     
        $("#selectList").append(new Option(response[i].doctor_name, response[i].doctor_name));
  
    }
  }
  })


}

function bookaNewVisit(){

  var patient_name=$("#patient_name").val();
  var date=$("#date").val();
  var time=$("#time").val();
  var doctor=$("#selectList").val();
   
  if(isEmpty(patient_name) || isEmpty(date) || isEmpty(time) || isEmpty(doctor)){
    alert("Please input a Value");
    throw("Please input a Value");
  }

  $.ajax({
    type: "GET",
    contentType: "application/json",
    url: 'http://localhost:3000/bookings?date='+date+'&time='+time+'&doctor='+doctor,
    dataType: "json",
    success:function(resp){
      if(resp.length==0){

        $.ajax({
          type: "POST",
          contentType: "application/json",
          url: 'http://localhost:3000/bookings',
          data:JSON.stringify({
            patient_name:patient_name,
            date:date,
            time:time,
            doctor:doctor,
            confirmed:"pending",
            patient_id: sessionStorage.getItem("patient_id")
             }),
          dataType: "json",
          success:function(){
            getDashboardData();
          }
        })

      }     
    

    else{

      alert("Please choose another time or date");
     }
    }
  })

   

}


function logout(){
 

  sessionStorage.removeItem("patient_id");
  sessionStorage.removeItem("patient_username");

  var newUrl = window.location.pathname.replace("views/patient_dashboard", "index");
  location.replace(newUrl);
}
