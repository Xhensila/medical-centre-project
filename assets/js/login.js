

function handleLogin(){

    var username  =   $("#username").val();
    var password  =   $("#password").val();

    if(isEmpty(username) || isEmpty(password)) {
        alert("You must fill in all of the fields!");
        throw("You must fill in all of the fields");
    }

    if(username=="admin" && password=="admin"){

         var newUrl = window.location.pathname.replace("login.html","manager_dashboard.html");
         location.replace(newUrl);
         
        }
else{

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: 'http://localhost:3000/users?username=' + username + '&password=' +password ,
        dataType: "json",
        success:function(response){
            console.log(response);
            if(response.length==0){
                alert("We don't recognize this account. Please try again.");
            }//if
            else{
                
                var id       = response[0].id;
                var username = response[0].username;
               
                sessionStorage.setItem("patient_id", id);
                sessionStorage.setItem("patient_username", username);

                var newUrl = window.location.pathname.replace("login.html","patient_dashboard.html");
                location.replace(newUrl);

            }//else

        }
    }) 
     
    }
}

function showSignup(){

    var newUrl = window.location.pathname.replace("login.html","signup.html");
    location.replace(newUrl);

}
