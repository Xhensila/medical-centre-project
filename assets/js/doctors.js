
$( document ).ready(function() {

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: 'http://localhost:3000/doctors',
        data: "",
        dataType: "json",
        success: function(response){
             
            if(response.length == 0){
                $(".list-group").append("<li class='list-group-item list-group-item-info'>There is no registered doctor yet.");
            }//if
            else{
                for(var i=0;i<response.length;i++){

                    var number = 1 + Math.floor(Math.random() * 5);
                    
                    var doctor_name    = response[i].doctor_name;
                    var specialization = response[i].specialization;

                    $(".list-group").append(" <div class='row doctors'> <div class='col-md-4 col-sm-12 data '><img class='photo' src='C:/Users/User/Desktop/medical_centre/assets/img/"+number+".png'> </div> <div class='col-md-8 col-sm-12 data content'><strong>Doctor: </strong>" + doctor_name + "  <br> <strong>Specialization: </strong>" + specialization + "</div> </div>  ");
                }

         }//else

        }
      });


});
