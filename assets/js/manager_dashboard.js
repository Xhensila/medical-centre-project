$( document ).ready(function() {

    getDashboardData();  
  
  })

  function getDashboardData(){
    $("#doctors").removeClass("active");
    $("#dashboard").addClass("active");

    $(".list-group").html("");
    $("#new_doctor").remove();
    $("#newDoctor").remove();
    $("#title").html("DASHBOARD");

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: 'http://localhost:3000/bookings?confirmed=pending',
        dataType: "json",
        success:function(response){
            if(response.length == 0){
        

                $(".list-group").append( "<li class='list-group-item list-group-item-info'>There is not any pending booking.</li>");
    
           
        }else{
            for(var i=0;i<response.length;i++){

                $(".list-group").append( "<li class='list-group-item list-group-item-info' id= "+ response[i].id + "> Patient: "+ response[i].patient_name + "   Date: " + response[i].date+ "    Time: "+ response[i].time + "   Doctor: "+ response[i].doctor+  "   <i class='fa fa-check-circle'></i>  <i class='fa fa-times'></i> </li>");
    
           }
        }
        $(".fa-check-circle").click(function(){

            var booking_id = $(this).parent().attr('id');

            approveVisit(booking_id);

       });

       $(".fa-times").click(function(){
        var id = $(this).parent().attr('id');
        dissapproveVisit(id);
       })

        }
    })

};

function approveVisit(booking_id){


    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: 'http://localhost:3000/bookings?id='+booking_id,
        dataType: "json",
        success:function(resp){
            var patient_name  =  resp[0].patient_name;
            var date          =  resp[0].date;
            var time          =  resp[0].time;
            var doctor        =  resp[0].doctor;
            var patient_id    =  resp[0].patient_id;
         

            $.ajax({
                type: "PUT",
                contentType: "application/json",
                url: 'http://localhost:3000/bookings/'+booking_id+'/',
                dataType: "json",
                data:JSON.stringify({
                    patient_name:patient_name,
                    patient_id:patient_id,
                    date:date,
                    time:time,
                    doctor:doctor,
                    confirmed:"1"

                }),
                success:function(){
                    getDashboardData();
                }
            })


        }
    })

}

function dissapproveVisit(id){

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: 'http://localhost:3000/bookings?id='+id,
        dataType: "json",
        success:function(resp){
            var patient_name  =  resp[0].patient_name;
            var date          =  resp[0].date;
            var time          =  resp[0].time;
            var doctor        =  resp[0].doctor;
            var patient_id    =  resp[0].patient_id;
         

            $.ajax({
                type: "PUT",
                contentType: "application/json",
                url: 'http://localhost:3000/bookings/'+id+'/',
                dataType: "json",
                data:JSON.stringify({
                    patient_name:patient_name,
                    patient_id:patient_id,
                    date:date,
                    time:time,
                    doctor:doctor,
                    confirmed:"0"

                }),
                success:function(){
                    getDashboardData();
                }
            })


        }
    })
}



function showDoctors(){

    $("#newDoctor").remove();
      
    $("#dashboard").removeClass("active");
    $("#doctors").addClass("active");
    $(".list-group").html("");
    $("#new_doctor").remove();

    $("#title").html("DOCTORS");
    
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: 'http://localhost:3000/doctors',
        data: "",
        dataType: "json",
        success: function(response){
            
            if(response.length == 0){
                $(".list-group").append("<li class='list-group-item list-group-item-info'>There are no registered doctors yet.</li>");
            }

            else{
                for(var i=0;i<response.length;i++){
                    var id = response[i].id;
                    var doctor_name=response[i].doctor_name;
                    var specialization=response[i].specialization;

                    $(".list-group").append("<li class='list-group-item list-group-item-info' > <strong>Doctor: </strong>"+ doctor_name + "   <strong>Specialization: </strong>" + specialization+ "   <i id=" + id+ " class='fa fa-times ' onclick='deleteDoctor(this.id)'></i></li>");
                }

        }
      }
    });

    $("#content").append('<br><button onclick="addNewDoctor();" id="newDoctor" type="button" class="btn btn-success">Add new doctor </button> ');

}

function deleteDoctor(id){
    $.ajax({
        type: "DELETE",
        contentType: "application/json",
        url: 'http://localhost:3000/doctors/' + id,
        data: "",
        dataType: "json",
        success:function(){
            showDoctors();
        }


    } )


    }


function addNewDoctor(){
    
    $(".list-group").html("");
    $("#newDoctor").remove();
    
    var html = "";
    html+='<div id="new_doctor">';
    html+='<div class=" row">' ;
    html+= ' <div class="col-sm-9 col-md-7 col-lg-5 mx-auto"> ';
    html+=' <div class="card card-signin my-5">'
    html+='  <div class="card-body">';
    html+='   <div class="form-label-group">'
    html+=' <input type="text" id="username" class="form-control" placeholder="Doctor name" required autofocus>';
    html+=' </div>';
    
    html+='   <div class="form-label-group">'
    html+=' <input type="text" id="specialization" class="form-control" placeholder="Specialization" required autofocus>';
    html+=' </div>';
    html+=' <div id="button">';
    html+='<button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" onclick="saveDoctor();" id="save_doctor">SAVE</button>';
    
    html+='  </div>';
    html+='  </div> ';
    html+='  </div> ';
    html+='  </div> ';
    html+='  </div> ';


    $("#content").append(html);
}

function saveDoctor(){
    var doctor_name=$("#username").val();
    var specialization=$("#specialization").val();


    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: 'http://localhost:3000/doctors/' ,
        data: JSON.stringify({ doctor_name: doctor_name, specialization:specialization}),
        dataType: "json",
        success:function(){
            showDoctors();
        }


    }) 
}



function showDashboard(){
    getDashboardData();
}






function logout(){

    var newUrl = window.location.pathname.replace("views/manager_dashboard", "index");
    location.replace(newUrl);

}